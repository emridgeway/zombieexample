﻿using TechTalk.SpecFlow;

namespace ZombieExample.AcceptanceTests.Steps {
    [Binding]
    public class PersonSteps {
        [Given(@"I have a living person")]
        public void givenIHaveALivingPerson() {
            Person person = new Person();
            ScenarioContext.Current["person"] = person;
        }
    }
}