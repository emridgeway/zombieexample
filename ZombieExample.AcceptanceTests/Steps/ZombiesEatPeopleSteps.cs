﻿using Should.Fluent;
using TechTalk.SpecFlow;

namespace ZombieExample.AcceptanceTests.Steps {
    [Binding]
    public class ZombiesEatPeopleSteps {
        [Given(@"I have an animated zombie")]
        public void givenIHaveAnAnimatedZombie() {
            Zombie zombie = new Zombie();
            ScenarioContext.Current["zombie"] = zombie;
        }

        [When(@"I eat a person")]
        public void whenIEatAPerson() {
            Zombie zombie = (Zombie)ScenarioContext.Current["zombie"];
            Person person = (Person)ScenarioContext.Current["person"];

            zombie.eat(person);
        }

        [Then(@"the person should die")]
        public void thenThePersonShouldDie() {
            Person person = (Person)ScenarioContext.Current["person"];

            person.isAlive.Should().Be.False();
        }

        [When(@"I attack a zombie")]
        public void whenIAttackAZombie() {
            Zombie zombie = (Zombie)ScenarioContext.Current["zombie"];
            Person person = (Person)ScenarioContext.Current["person"];

            person.attack(zombie);
        }

        [Then(@"the zombie should not die")]
        public void thenTheZombieShouldNotDie() {
            Zombie zombie = (Zombie) ScenarioContext.Current["zombie"];

            zombie.isAnimated.Should().Be.True();
        }

        [Then(@"the zombie should die")]
        public void thenTheZombieShouldDie() {
            Zombie zombie = (Zombie)ScenarioContext.Current["zombie"];

            zombie.isAnimated.Should().Be.False();
        }
    }
}