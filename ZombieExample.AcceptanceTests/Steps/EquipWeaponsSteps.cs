﻿using Should.Fluent;
using TechTalk.SpecFlow;

namespace ZombieExample.AcceptanceTests.Steps {

    [Binding]
    public class EquipWeaponsSteps {
        [Given(@"I do not have an equipped weapon")]
        public void givenIDoNotHaveAnEquippedWeapon() {
            Person person = (Person)ScenarioContext.Current["person"];

            person.unequipWeapon();
        }

        [When(@"I equip a weapon")]
        public void whenIEquipAWeapon() {
            Person person = (Person)ScenarioContext.Current["person"];

            person.equipWeapon();
        }

        [Then(@"I should have an active weapon")]
        public void thenIShouldHaveAnActiveWeapon() {
            Person person = (Person)ScenarioContext.Current["person"];

            person.equippedWeapon.Should().Not.Be.Null();
            person.equippedWeapon.weaponType.Should().Equal(Weapon.WeaponType.Armed);
        }

        [Given(@"I have an equipped weapon")]
        public void givenIHaveAnEquippedWeapon() {
            Person person = (Person)ScenarioContext.Current["person"];

            person.equipWeapon();
        }
    }
}