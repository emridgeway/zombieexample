﻿Feature: People attack zombies
	In order to avoid being eaten
	As a person
	I want to kill zombies

Scenario: Person attacks zombie without a weapon
	Given I have a living person
	And I do not have an equipped weapon
	And I have an animated zombie
	When I attack a zombie
	Then the zombie should not die

Scenario: Person attacks zombie with a weapon
	Given I have a living person
	And I have an equipped weapon
	And I have an animated zombie
	When I attack a zombie
	Then the zombie should die