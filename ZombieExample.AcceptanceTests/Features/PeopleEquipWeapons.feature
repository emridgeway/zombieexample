﻿Feature: People equip weapons
	In order to protect myself from zombies
	As a person
	I want to use a weapon

Scenario: Person equips a weapon
	Given I have a living person
	And I do not have an equipped weapon
	When I equip a weapon
	Then I should have an active weapon