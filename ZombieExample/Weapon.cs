namespace ZombieExample {

    public class Weapon {
        private readonly WeaponType _weaponType;

        public Weapon(WeaponType type) {
            _weaponType = type;
        }

        public WeaponType weaponType {
            get { return _weaponType; }
        }

        public enum WeaponType {
            Unarmed
            , Armed
        }

    }
}
