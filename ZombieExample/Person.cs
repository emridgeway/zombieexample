using System;

namespace ZombieExample {
    public class Person {
        private Boolean _isAlive;
        private Weapon _equippedWeapon;

        public Person() {
            _isAlive = true;
            _equippedWeapon = new Weapon(Weapon.WeaponType.Unarmed);
        }

        public void die() {
            _isAlive = false;
        }

        public void attack(Zombie zombie) {
            if (equippedWeapon.weaponType == Weapon.WeaponType.Armed) {
                zombie.die();
            }
        }

        public void equipWeapon() {
            _equippedWeapon = new Weapon(Weapon.WeaponType.Armed);
        }

        public void unequipWeapon() {
            _equippedWeapon = new Weapon(Weapon.WeaponType.Unarmed);
        }

        /*
         * Properties
         */
        public Weapon equippedWeapon {
            get { return _equippedWeapon; }
        }

        public Boolean isAlive {
            get { return _isAlive; }
        }
    }
}