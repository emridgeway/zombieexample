using System;

namespace ZombieExample {
    public class Zombie {
        private Boolean _isAnimated;

        public Zombie() {
            _isAnimated = true;
        }

        public void eat(Person person) {
            person.die();
        }

        public void die() {
            _isAnimated = false;
        }   
    
        /*
         * Properties
         */
        public Boolean isAnimated {
            get { return _isAnimated; }
        }

        /*
         * todo Note for example: I originally had Setter for isAnimated, and setters
         * for a lot of the properties in the other classes. I decided I didn't want any
         * setters if I could help it, I wanted .die() etc to control the interactions
         * with those states instead.
         * This was a refactoring. Normally I'd have been scared to make such a sweeping
         * change, because I'd be worried I was breaking something else.
         * Instead, I was able to do it fearlessly, because tests would show me
         * what I broke and confirm everything was working once I finished refactoring
         * to my heart's content
         */





    }
}
