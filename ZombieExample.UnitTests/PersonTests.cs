﻿using NUnit.Framework;
using Should.Fluent;

namespace ZombieExample.UnitTests {

    [TestFixture]
    public class PersonTests {
        [Test]
        public void peopleShouldStartOutAlive() {
            Person person = new Person();

            person.isAlive.Should().Be.True();
        }

        [Test]
        public void peopleStartUnarmed() {
            Person person = new Person();

            person.equippedWeapon.Should().Not.Be.Null();
            person.equippedWeapon.weaponType.Should().Equal(Weapon.WeaponType.Unarmed);
        }

        [Test]
        public void afterDieingAPersonShouldntBeAlive() {
            Person person = new Person();

            person.die();

            person.isAlive.Should().Be.False();
        }

        [Test]
        public void peopleCanEquipWeapons() {
            Person person = new Person();

            person.equipWeapon();

            person.equippedWeapon.weaponType.Should().Equal(Weapon.WeaponType.Armed);
        }

        [Test]
        public void peopleCanUnequipWeapons() {
            Person person = new Person();

            person.unequipWeapon();

            person.equippedWeapon.weaponType.Should().Equal(Weapon.WeaponType.Unarmed);
        }

        [Test]
        public void personWithWeaponAttackingLiveZombieShouldKillIt() {
            Person person = new Person();
            Zombie zombie = new Zombie();

            person.equipWeapon();
            person.attack(zombie);

            person.equippedWeapon.weaponType.Should().Equal(Weapon.WeaponType.Armed);
            zombie.isAnimated.Should().Be.False();
        }

        [Test]
        public void personWithoutWeaponAttackLiveZombieShouldNowKillIt() {
            Person person = new Person();
            Zombie zombie = new Zombie();

            person.unequipWeapon();
            person.attack(zombie);

            person.equippedWeapon.weaponType.Should().Not.Equal(Weapon.WeaponType.Armed);
            zombie.isAnimated.Should().Be.True();   
        }
    }

 
}
