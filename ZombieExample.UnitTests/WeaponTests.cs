﻿using NUnit.Framework;
using Should.Fluent;

namespace ZombieExample.UnitTests {
    [TestFixture]
    public class WeaponTests {
        [Test]
        public void weaponsShouldBeACertainType() {
            Weapon.WeaponType[] someTypes = new Weapon.WeaponType[] {
                Weapon.WeaponType.Unarmed
                , Weapon.WeaponType.Armed
            };

            foreach (Weapon.WeaponType type in someTypes) {
                Weapon weapon = new Weapon(type);

                type.Should().Equal(weapon.weaponType);
            }
        }

        //        [Test]
        //        public void tryingToChangeWeaponTypeShouldError() {
        //            Weapon weapon = new Weapon(Weapon.WeaponType.Unarmed);
        //
        //            Assert.Throws(typeof (Exception), delegate {
        //                weapon.WeaponType = Weapon.WeaponType.Armed;
        //            });
        //        }

        // todo read the weapon WeaponType
    }
}
