﻿using NUnit.Framework;
using Should.Fluent;

namespace ZombieExample.UnitTests {
    [TestFixture]
    public class ZombieTests {
        [Test]
        public void zombiesShouldStartOutAnimated() {
            Zombie zombie = new Zombie();

            zombie.isAnimated.Should().Be.True();
        }

        [Test]
        public void afterDieingItShouldntBeAnimated() {
            Zombie zombie = new Zombie();

            zombie.die();

            zombie.isAnimated.Should().Be.False();
        }
    }
}
